﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class CommentController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Comment
        public ActionResult Index()
        {
            return View();
        }

        // GET: Comment/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult LikeComment(int id)
        {
            int liker = Int32.Parse(Session["account_id"].ToString());
            Models.comment_likes newCommentLike = new Models.comment_likes()
            {
                person_id = liker,
                comment_id = id,
                timestamp = DateTime.Now,
                read = 0
            };
            db.comment_likes.Add(newCommentLike);
            db.SaveChanges();
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult UnlikeComment(int id)
        {
            int liker = Int32.Parse(Session["account_id"].ToString());
            Models.comment_likes theLike = db.comment_likes.SingleOrDefault(l => l.comment_id == id && l.person_id == liker);
            db.comment_likes.Remove(theLike);
            db.SaveChanges();
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        // GET: Comment/Create
        public ActionResult Create(int id)
        {
            int sender = Int32.Parse(Session["account_id"].ToString());
            ViewBag.id = id;
            return View();
        }

        // POST: Comment/Create
        [HttpPost]
        public ActionResult Create(int id, FormCollection collection)
        {
            int sender = Int32.Parse(Session["account_id"].ToString());
            try
            {
                Models.comment newComment = new Models.comment()
                {
                    picture_id = id,
                    profile_id = sender,
                    comment_text = collection["comment_text"],
                    timestamp = DateTime.Now,
                    read = 0
                };
                db.comments.Add(newComment);
                db.SaveChanges();

                return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
            }
            catch
            {
                return View();
            }
        }

        // GET: Comment/Edit/5
        public ActionResult Edit(int id)
        {
            Models.comment theComment = db.comments.SingleOrDefault(c => c.comment_id == id);
            if (theComment.profile_id != Int32.Parse(Session["account_id"].ToString()))
            {
                return RedirectToAction("Index", "Home");
            }
            return View(theComment);
        }

        // POST: Comment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                Models.comment theComment = db.comments.SingleOrDefault(c => c.comment_id == id);
                theComment.comment_text = collection["comment_text"];
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }
        
        // GET: Comment/Delete/5
        public ActionResult Delete(int id)
        {
            Models.comment theComment = db.comments.SingleOrDefault(c => c.comment_id == id);
            if (theComment.profile_id != Int32.Parse(Session["account_id"].ToString()))
            {
                return RedirectToAction("Index", "Home");
            }
            IEnumerable<Models.comment_likes> theLikes = db.comment_likes.Where(c => c.comment_id == id);
            foreach (Models.comment_likes x in theLikes) {
                db.comment_likes.Remove(x);
            }
            db.comments.Remove(theComment);
            db.SaveChanges();
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }
    }
}
