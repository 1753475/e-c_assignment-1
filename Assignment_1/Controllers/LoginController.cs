﻿using Assignment_1.ActionFilters;
using Assignment_1.Content;
using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    public class LoginController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Login
        [LoggedIn]
        public ActionResult Index()
        {
            return View();
        }

        [LoggedIn]
        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            string username = collection["username"];
            Models.account theAccount = db.accounts.SingleOrDefault(u => u.username.Equals(username));
            if (theAccount != null &&
                Crypto.VerifyHashedPassword(theAccount.password_hash, collection["password_hash"]))
            {
                Session["profile_id"] = theAccount.profile_id;
                if (theAccount.secret != null)
                {
                    return RedirectToAction("Validation");
                }
                else{
                    Session["account_id"] = theAccount.account_id;
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                ViewBag.error = 1;
                return View();
            }
        }

        [LoggedIn]
        public ActionResult Validation()
        {
            if (Session["profile_id"] == null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        [LoggedIn]
        [HttpPost]
        public ActionResult Validation(FormCollection collection)
        {
            if (Session["profile_id"] == null) {
                return RedirectToAction("Index");
            }
            int id = Int32.Parse(Session["profile_id"].ToString());
            Models.account theAccount = db.accounts.SingleOrDefault(a => a.account_id == id);
            if (theAccount != null)
            {
                if (theAccount.secret != null)
                {
                    Totp totp = new Totp(theAccount.secret);
                    string theCode = totp.AuthenticationCode;
                    if (theCode.Equals(collection["secret"]))
                    {
                        Session["account_id"] = theAccount.account_id;
                        return RedirectToAction("Index", "Home");
                    }
                }
                ViewBag.error = 1;
                return View();
            }
            else
            {
                ViewBag.error = "An Error has occured!";
                return View();
            }
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index");
        }

        public static Random random = new Random();

        private static string RandomBase32String(int lenght) {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
            return new string(Enumerable.Repeat(chars, lenght).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        // GET: ChangePassword/
        [LoginFilter]
        public ActionResult ChangePassword()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            ViewBag.id = id;
            ViewBag.name = db.clients.SingleOrDefault(c => c.person_id == id).first_name + " " +
                           db.clients.SingleOrDefault(c => c.person_id == id).last_name;
            return View();
        }

        // POST: Login/ChangePassword
        [HttpPost]
        public ActionResult ChangePassword(FormCollection collection)
        {
            try
            {
                int id = Int32.Parse(Session["account_id"].ToString());
                Models.account theAccount = db.accounts.SingleOrDefault(c => c.account_id == id);
                if (theAccount != null &&
                    Crypto.VerifyHashedPassword(theAccount.password_hash, collection["password_hash"]))
                {
                    theAccount.password_hash = Crypto.HashPassword(collection["password_hash_new"]);
                    db.SaveChanges();
                    return RedirectToAction("Details", "Profile", new { id = theAccount.account_id });
                }
                else
                {
                    ViewBag.error = 1;
                    return View();
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        [LoggedIn]
        public ActionResult Register()
        {
            string secret = RandomBase32String(16);
            string otpauth = "otpauth://totp/User:E-Commerce?secret="+ secret +"&issuer=SocialNetwork_";

            //Generates a QR code
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(otpauth, QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);
            ImageConverter converter = new ImageConverter();
            ViewBag.QRCode = (byte[])converter.ConvertTo(qrCodeImage, typeof(byte[]));
            Session["secret"] = secret;

            return View();
        }

        // POST: Login/Register
        [LoggedIn]
        [HttpPost]
        public ActionResult Register(FormCollection collection)
        {
            try
            {
                string secret = Session["secret"].ToString();
                Totp totp = new Totp(secret);

                String theSecret = null;
                if (totp.AuthenticationCode.Equals(collection["validation"].Trim())) {
                    //add secret to user account
                    theSecret = secret;
                }

                string username = collection["username"];
                Models.account theAccount = db.accounts.SingleOrDefault(u => u.username.Equals(username));
                if (theAccount != null)
                {
                    return RedirectToAction("Register");
                }

                Models.account newAccount = new Models.account()
                {
                    username = collection["username"],
                    password_hash = Crypto.HashPassword(collection["password_hash"]),
                    secret = theSecret
                };

                db.accounts.Add(newAccount);
                db.SaveChanges();
                Session["account_id"] = newAccount.account_id;
                return RedirectToAction("Create", "Profile");
            }
            catch {
                return View();
            }
        }
    }
}
