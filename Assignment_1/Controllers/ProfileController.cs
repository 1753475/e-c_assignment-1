﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    [LoginFilter]
    public class ProfileController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Home
        public ActionResult Index()
        {
            return View(db.clients);
        }

        public ActionResult Search(String name) {
            IEnumerable<Models.client> result = db.clients.Where(c => (c.first_name + " " + c.last_name).Contains(name));
            return View("Index", result);
        }

        [ProfileNeeded]
        public ActionResult Details(int id)
        {
            Models.client theClient = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(theClient);
        }

        // GET: Home/Create
        [ProfileExists]
        public ActionResult Create()
        {
            ViewBag.gender = new List<SelectListItem>() {
                new SelectListItem { Text = "Male", Value = "M" },
                new SelectListItem { Text = "Female", Value = "F" }
            };
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                int id = Int32.Parse(Session["account_id"].ToString());
                Byte flag = 0;
                if (collection["privacyFlag"] != null)
                {
                    flag = Byte.Parse(collection["privacyFlag"]);
                }

                Models.client newClient = new Models.client()
                {
                    person_id = id,
                    first_name = collection["first_name"],
                    last_name = collection["last_name"],
                    notes = collection["notes"],
                    gender = collection["gender"],
                    privacy_flag = flag
                };
                db.clients.Add(newClient);
                db.SaveChanges();

                Models.account theAccount = db.accounts.SingleOrDefault(c => c.account_id == newClient.person_id);
                theAccount.profile_id = id;
                db.SaveChanges();

                Session["profile_id"] = theAccount.profile_id;

                return RedirectToAction("Details", "Profile", new {id = newClient.person_id});
            }
            catch
            {
                return View();
            }
        }

        [ProfileNeeded]
        // GET: Home/Edit/5
        public ActionResult Edit()
        {
            ViewBag.gender = new List<SelectListItem>() {
                new SelectListItem { Text = "Male", Value = "M" },
                new SelectListItem { Text = "Female", Value = "F" }
            };
            int id = Int32.Parse(Session["account_id"].ToString());
            ViewBag.profile_pics = db.pictures.Where(p => p.person_id == id).Select(p => new SelectListItem() { Value = p.picture_id.ToString(), Text = p.caption});
            Models.client theClient = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(theClient);
        }

        // POST: Home/Edit/5
        [HttpPost]
        public ActionResult Edit(FormCollection collection)
        {
            try
            {
                int id = Int32.Parse(Session["account_id"].ToString());
                Models.client theClient = db.clients.SingleOrDefault(c => c.person_id == id);
                theClient.first_name = collection["first_name"];
                theClient.last_name = collection["last_name"];
                theClient.notes = collection["notes"];
                theClient.gender = collection["gender"];
                if (collection["privacyFlag"] == null) {
                    theClient.privacy_flag = 0;
                }
                else {
                    theClient.privacy_flag = Byte.Parse(collection["privacyFlag"]);
                }
                if (collection["profile_picture"] != null) {
                    theClient.profile_picture = Int32.Parse(collection["profile_picture"]);
                }
                
                db.SaveChanges();
                return RedirectToAction("Details", new {id = theClient.person_id});
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
        }

        // GET: Home/Delete/5
        [ProfileNeeded]
        public ActionResult Delete()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            Models.client theClient = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(theClient);
        }

        // POST: Home/Delete/5
        [HttpPost]
        public ActionResult Delete(FormCollection collection)
        {
            try
            {
                int id = Int32.Parse(Session["account_id"].ToString());
                Models.client theClient = db.clients.Find(id);
                Models.account theAccount = db.accounts.Find(id);
                db.pictures.RemoveRange(theClient.pictures);
                theClient.profile_picture = null;
                db.SaveChanges();

                db.addresses.RemoveRange(theClient.addresses);
                db.SaveChanges();

                db.clients.Remove(theClient);
                db.SaveChanges();

                db.accounts.Remove(theAccount);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                return View();
            }
        }
    }
}
