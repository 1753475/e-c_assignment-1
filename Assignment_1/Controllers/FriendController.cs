﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    [LoginFilter]
    [ProfileNeeded]
    public class FriendController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Friend
        public ActionResult Index()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            Models.client theClient = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(theClient);
        }

        public ActionResult Search(string data)
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            IEnumerable<Models.client> result = db.clients.Where(c => (c.first_name + " " + c.last_name).Contains(data));
            return View("SearchResults", result);
        }

        // GET: Friend/New
        public ActionResult New(int id)
        {
            ViewBag.id = id;
            ViewBag.name = db.clients.SingleOrDefault(c => c.person_id == id).first_name + " " +
                           db.clients.SingleOrDefault(c => c.person_id == id).last_name;
            return View();
        }

        // POST: Friend/New
        [HttpPost]
        public ActionResult New(int id, FormCollection collection)
        {
            try
            {
                int idd = Int32.Parse(Session["account_id"].ToString());
                Models.friendlink newRequest = new Models.friendlink()
                {
                    requester = idd,
                    requested = id,
                    status = collection["status"],
                    timestamp = DateTime.Now,
                    read = 0,
                    approved = 0
                };
                db.friendlinks.Add(newRequest);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            catch {
                return View();
            }
        }

        public ActionResult Accept(int id)
        {
            int reqsted = Int32.Parse(Session["account_id"].ToString());
            Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requester == id && f.requested == reqsted);
            if (theFriend.approved == 1) {
                return Content("Already Friends");
            }
            theFriend.read = 1;
            theFriend.approved = 1;

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            int reqsted = Int32.Parse(Session["account_id"].ToString());
            Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requester == id && f.requested == reqsted);
            db.friendlinks.Remove(theFriend);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Cancel(int id)
        {
            int reqsted = Int32.Parse(Session["account_id"].ToString());
            Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requested == id && f.requester == reqsted);
            db.friendlinks.Remove(theFriend);

            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Friend/Edit/5
        public ActionResult Edit(int id)
        {
            Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requested == id);
            return View(theFriend);
        }

        // POST: Friend/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                int idd = Int32.Parse(Session["account_id"].ToString());
                Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requested == id);
                theFriend.status = collection["status"];

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Friend/Remove/5
        public ActionResult Remove(int id)
        {
            int idd = Int32.Parse(Session["account_id"].ToString());
            Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requested == id && f.requester == idd || f.requested == idd && f.requester == id);
            return View(theFriend);
        }

        // POST: Friend/Remove/5
        [HttpPost]
        public ActionResult Remove(int id, FormCollection collection)
        {
            try
            {
                int idd = Int32.Parse(Session["account_id"].ToString());
                Models.friendlink theFriend = db.friendlinks.SingleOrDefault(f => f.requested == id && f.requester == idd || f.requested == idd && f.requester == id);
                db.friendlinks.Remove(theFriend);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
