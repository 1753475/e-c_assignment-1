﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    [LoginFilter]
    public class AddressController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Address
        public ActionResult Index(int id)
        {
            ViewBag.id = id;
            var theAddresses = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(theAddresses);
        }

        // GET: Address/Details/5
        public ActionResult Details(int id)
        {
            Models.address theAddress = db.addresses.SingleOrDefault(a => a.address_id == id);
            return View(theAddress);
        }

        // GET: Address/Create
        public ActionResult Create(int id)
        {
            ViewBag.id = id;
            ViewBag.countries = db.countries.Select(c => new SelectListItem() { Value = c.country_id.ToString(), Text = c.country_name });
            return View();
        }

        // POST: Address/Create
        [HttpPost]
        public ActionResult Create(int id, FormCollection collection)
        {
            try
            {
                Models.address newAddress = new Models.address()
                {
                    person_id = id,
                    description = collection["description"],
                    street_address = collection["street_address"],
                    city = collection["city"],
                    province = collection["province"],
                    postal_code = collection["postal_code"],
                    country_id = Int32.Parse(collection["country_id"])
                };

                db.addresses.Add(newAddress);
                db.SaveChanges();
                return RedirectToAction("Details", "Profile", new { id = newAddress.person_id });
            }
            catch
            {
                return View();
            }
        }

        // GET: Address/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.countries = db.countries.Select(c => new SelectListItem() { Value = c.country_id.ToString(), Text = c.country_name });
            Models.address theAddress = db.addresses.SingleOrDefault(a => a.person_id == id);
            return View(theAddress);
        }

        // POST: Address/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                Models.address theAddress = db.addresses.SingleOrDefault(a => a.person_id == id);
                theAddress.description = collection["description"];
                theAddress.street_address = collection["street_address"];
                theAddress.city = collection["city"];
                theAddress.province = collection["province"];
                theAddress.postal_code = collection["postal_code"];
                theAddress.country_id = Int32.Parse(collection["country_id"]);

                db.SaveChanges();
                return RedirectToAction("Details", "Profile", new { id = theAddress.person_id });
            }
            catch
            {
                return View();
            }
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int id)
        {
            Models.address theAddress = db.addresses.SingleOrDefault(a => a.address_id == id);
            return View(theAddress);
        }

        // POST: Address/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Models.address theAddress = db.addresses.Find(id);
                db.addresses.Remove(theAddress);
                db.SaveChanges();
                return RedirectToAction("Details", "Profile", new { id = theAddress.person_id });
            }
            catch
            {
                return View();
            }
        }
    }
}
