﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    [LoginFilter]
    public class MessageController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Message
        public ActionResult Index()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            Models.client theClient = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(theClient);
        }

        // GET: Message/Details/5
        public ActionResult Details()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            Models.message theMessages = db.messages.SingleOrDefault(m => m.receiver_id == id);
            return View(theMessages);
        }

        public ActionResult Private(int id) {
            ViewBag.id = id;
            int sender = Int32.Parse(Session["account_id"].ToString());
            
            ViewBag.name = db.clients.SingleOrDefault(c => c.person_id == id).first_name + " " +
                           db.clients.SingleOrDefault(c => c.person_id == id).last_name;
            IEnumerable<Models.message> theConversation1 = db.messages.Where(m => m.sender_id == sender && m.receiver_id == id);
            IEnumerable<Models.message> theConversation2 = db.messages.Where(m => m.receiver_id == sender && m.sender_id == id);
            IEnumerable<Models.message> theConversation = theConversation1.Concat(theConversation2).OrderBy(m => m.timestamp);
            IEnumerable<Models.message> read = db.messages.Where(m => m.sender_id == id && m.receiver_id == sender);
            foreach (Models.message x in read)
            {
                x.read = 1;
            }
            db.SaveChanges();
            return View(theConversation);
        }

        public ActionResult NewMessage(int id, FormCollection collection)
        {
            int sender = Int32.Parse(Session["account_id"].ToString());
            try
            {
                Models.message newMessage = new Models.message()
                {
                    sender_id = sender,
                    receiver_id = id,
                    message_text = collection["message_text"],
                    timestamp = DateTime.Now,
                    read = 0
                };
                db.messages.Add(newMessage);
                db.SaveChanges();
                return RedirectToAction("Private", "Message", new { id = newMessage.receiver_id });
            }
            catch{
                return View();
            }
        }

        // GET: Message/New
        public ActionResult New()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            IEnumerable<SelectListItem> friends1 = db.friendlinks.Where(f => f.requested == id || f.requester == id && f.approved == 1)
                .Select(f => new SelectListItem() { Value = f.requester.ToString(), Text = (f.client.first_name) + " " + f.client.last_name });
            IEnumerable<SelectListItem> friends2 = db.friendlinks.Where(f => f.requester == id && f.approved == 1)
                .Select(f => new SelectListItem() { Value = f.requester.ToString(), Text = (f.client1.first_name) + " " + f.client1.last_name });
            ViewBag.friends = friends1.Concat(friends2);
            return View();
        }

        // POST: Message/New
        [HttpPost]
        public ActionResult New(FormCollection collection)
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            try
            {
                Models.message newMessage = new Models.message() {
                    sender_id = id,
                    receiver_id = Int32.Parse(collection["receiver_id"]),
                    message_text = collection["message_text"],
                    timestamp = DateTime.Now,
                    read = 0
                };

                db.messages.Add(newMessage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch{
                return View();
            }
        }

        // GET: Message/Delete/
        public ActionResult Delete(int id)
        {
            Models.message theMessage = db.messages.SingleOrDefault(m => m.message_id == id);
            db.messages.Remove(theMessage);
            db.SaveChanges();
            return RedirectToAction("Private", new { id = theMessage.receiver_id });
        }
    }
}
