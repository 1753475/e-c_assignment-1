﻿using Assignment_1.ActionFilters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.Controllers
{
    [LoginFilter]
    public class HomeController : Controller
    {
        Models.ClientsDBEntities db = new Models.ClientsDBEntities();
        // GET: Picture
        public ActionResult Index()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            ViewBag.id = id;
            var thePictures = db.clients.SingleOrDefault(c => c.person_id == id);
            return View(thePictures);
        }

        public ActionResult Like(int id)
        {
            int liker = Int32.Parse(Session["account_id"].ToString());
            Models.like newLike = new Models.like() {
                person_id = liker,
                picture_id = id,
                timestamp = DateTime.Now,
                read = 0
            };
            db.likes.Add(newLike);
            db.SaveChanges();
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult Unlike(int id)
        {
            int liker = Int32.Parse(Session["account_id"].ToString());
            Models.like theLike = db.likes.SingleOrDefault(l => l.picture_id == id && l.person_id == liker);
            db.likes.Remove(theLike);
            db.SaveChanges();
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult HomeList()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            ViewBag.id = id;
            IEnumerable<Models.picture> thePictures = db.pictures.Where(p => p.person_id == id);
            return View(thePictures);
        }
    // GET: Picture/Create
        public ActionResult Create()
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            ViewBag.id = id;
            return View();
        }
      
        // POST: Picture/Create/5
        [HttpPost]
        public ActionResult Create(FormCollection collection, HttpPostedFileBase newPicture)
        {
            int id = Int32.Parse(Session["account_id"].ToString());
            try
            {
                string[] type = { "image/gif", "image/png", "image/jpeg" };

                if (newPicture != null && newPicture.ContentLength > 0 && type.Contains(newPicture.ContentType))
                {
                    Guid g = Guid.NewGuid();
                    string filename = g + Path.GetExtension(newPicture.FileName);
                    string path = Server.MapPath("~/Images/");
                    path = Path.Combine(path, filename);
                    newPicture.SaveAs(path);

                    Models.picture newPic = new Models.picture()
                    {
                        caption = collection["caption"],
                        time = collection["time"],
                        location = collection["location"],
                        relative_path = filename,
                        person_id = id
                    };
                    
                    db.pictures.Add(newPic);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Picture/Edit/5
        public ActionResult Edit(int id)
        {
            Models.picture thePicture = db.pictures.SingleOrDefault(p => p.picture_id == id);
            if (thePicture.person_id != Int32.Parse(Session["account_id"].ToString()))
            {
                return RedirectToAction("Index", "Home");
            }
            return View(thePicture);
        }

        // POST: Picture/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                Models.picture thePicture = db.pictures.SingleOrDefault(p => p.picture_id == id);
                thePicture.caption = collection["caption"];
                thePicture.relative_path = collection["relative_path"];
                thePicture.time = collection["time"];
                thePicture.location = collection["location"];

                db.SaveChanges();
                return RedirectToAction("Index", new { id = thePicture.person_id});
            }
            catch
            {
                return View();
            }
        }

        // GET: Picture/Delete/5
        public ActionResult Delete(int id)
        {
            Models.picture thePicture = db.pictures.SingleOrDefault(p => p.picture_id == id);
            if (thePicture.person_id != Int32.Parse(Session["account_id"].ToString()))
            {
                return RedirectToAction("Index", "Home");
            }
            return View(thePicture);
        }

        // POST: Picture/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                string path = Server.MapPath("~/Images/");
                Models.picture thePicture = db.pictures.SingleOrDefault(p => p.picture_id == id);
                path = Path.Combine(path, thePicture.relative_path);
                System.IO.File.Delete(path);

                db.pictures.Remove(thePicture);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
