﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assignment_1.Models
{
    [MetadataType(typeof(client_Validation))]
    public partial class client {
        //empty class, just added the annotation
    }

    //[Bind(Exclude = 'ID')]
    public class client_Validation
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter a name")]
        [StringLength(50, MinimumLength = 2)]
        public string first_name;

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter a last name")]
        [StringLength(50, MinimumLength = 2)]
        public string last_name;

        [Display(Name = "Biography")]
        [MaxLength(50)]
        public string notes;

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please select Male or Female")]
        public string gender;

        [Display(Name = "Profile Picture")]
        public string profile_picture;

        [Display(Name = "Privacy")]
        public string privacy_flag;
    }

    [MetadataType(typeof(picture_Validation))]
    public partial class picture {
        //empty class, just added the annotation
    }

    public class picture_Validation 
    {
        [Display(Name = "Relative Path")]
        [Required(ErrorMessage = "Relative Path required.")]
        [StringLength(1024, MinimumLength = 2)]
        public string relative_path;

        [Display(Name = "Caption")]
        [Required(ErrorMessage = "Please enter a caption.")]
        [StringLength(50, MinimumLength = 2)]
        public string caption;

        [Display(Name = "Time")]
        [Required(ErrorMessage = "Please enter a time.")]
        [StringLength(50, MinimumLength = 2)]
        public string time;

        [Display(Name = "Location")]
        [Required(ErrorMessage = "Please enter a location.")]
        [StringLength(50, MinimumLength = 2)]
        public string location;
    }

    [MetadataType(typeof(address_Validation))]
    public partial class address
    {
        //empty class, just added the annotation
    }

    public class address_Validation
    {
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Please enter a Description.")]
        [StringLength(50, MinimumLength = 2)]
        public string description;

        [Display(Name = "Street Address")]
        [Required(ErrorMessage = "Please enter a Street Address.")]
        [StringLength(50, MinimumLength = 2)]
        public string street_address;

        [Display(Name = "City")]
        [Required(ErrorMessage = "Please enter a City.")]
        [StringLength(50, MinimumLength = 2)]
        public string city;

        [Display(Name = "Province")]
        [Required(ErrorMessage = "Please enter a Province.")]
        [StringLength(50, MinimumLength = 2)]
        public string province;
        
        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please enter a Postal Code.")]
        [StringLength(50, MinimumLength = 2)]
        public string postal_code;
        
        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please select a country.")]
        public string country_id;
    }

    [MetadataType(typeof(country_Validation))]
    public partial class country
    {
        //empty class, just added the annotation
    }

    public class country_Validation
    {
        [Display(Name = "Country")]
        public string country_name;
    }

    [MetadataType(typeof(account_Validation))]
    public partial class account
    {
        //empty class, just added the annotation
    }

    public class account_Validation
    {
        [Display(Name = "Username")]
        [Required(ErrorMessage = "Please enter a username.")]
        [StringLength(50, MinimumLength = 2)]
        public string username;

        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter a secure password.")]
        public string password_hash;

        [Display(Name = "2FA Secret")]
        public string secret;
    }

    [MetadataType(typeof(message_Validation))]
    public partial class message
    {
        //empty class, just added the annotation
    }

    public class message_Validation
    {
        [Display(Name = "Friend")]
        [Required(ErrorMessage = "Please select a friend.")]
        public string receiver_id;

        [Display(Name = "Message")]
        [Required(ErrorMessage = "Please enter a message.")]
        [MinLength(1)]
        public string message_text;

        [Display(Name = "Time")]
        public string timestamp;

        [Display(Name = "Status")]
        public string read;
    }

    [MetadataType(typeof(friendlink_Validation))]
    public partial class friendlink
    {
        //empty class, just added the annotation
    }

    public class friendlink_Validation
    {
        [Display(Name = "Status")]
        [Required(ErrorMessage = "Please enter a status.")]
        [StringLength(50, MinimumLength = 2)]
        public string status;

        [Display(Name = "Time")]
        public string timestamp;

        [Display(Name = "Read")]
        public string read;

        [Display(Name = "Approved")]
        public string approved;
    }

    [MetadataType(typeof(comment_Validation))]
    public partial class comment
    {
        //empty class, just added the annotation
    }

    public class comment_Validation
    {
        [Display(Name = "Comment")]
        [Required(ErrorMessage = "Enter a comment.")]
        [MinLength(1)]
        public string comment_text;

        [Display(Name = "Time")]
        public string timestamp;

        [Display(Name = "Read")]
        public string read;
    }
}