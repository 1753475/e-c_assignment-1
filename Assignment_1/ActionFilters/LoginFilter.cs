﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.ActionFilters
{
    public class LoginFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if the person has not logged in
            if (filterContext.HttpContext.Session["account_id"] == null)
            {
                System.Web.Routing.RouteValueDictionary routeValues = new System.Web.Routing.RouteValueDictionary();
                routeValues.Add("controller", "Login");
                routeValues.Add("action", "Index");

                filterContext.Result = new RedirectToRouteResult("Default", routeValues);
            }
        }
    }
}