﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.ActionFilters
{
    public class LoggedIn : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if the person is already logged in
            if (filterContext.HttpContext.Session["account_id"] != null &&
                filterContext.HttpContext.Session["profile_id"] != null)
            {
                System.Web.Routing.RouteValueDictionary routeValues = new System.Web.Routing.RouteValueDictionary();
                routeValues.Add("controller", "Home");
                routeValues.Add("action", "Index");

                filterContext.Result = new RedirectToRouteResult("Default", routeValues);
            }
        }
    }
}