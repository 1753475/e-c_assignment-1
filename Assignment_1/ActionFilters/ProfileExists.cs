﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignment_1.ActionFilters
{
    public class ProfileExists : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //if the person already has a profile
            if (filterContext.HttpContext.Session["profile_id"] != null)
            {
                System.Web.Routing.RouteValueDictionary routeValues = new System.Web.Routing.RouteValueDictionary();
                routeValues.Add("controller", "Profile");
                routeValues.Add("action", "Details");
                routeValues.Add("id", filterContext.HttpContext.Session["account_id"]);

                filterContext.Result = new RedirectToRouteResult("Default", routeValues);
            }
        }
    }
}